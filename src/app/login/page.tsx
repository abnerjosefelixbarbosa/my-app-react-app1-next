"use client"

import style from "@/app/login/page.module.css"
import useMain from "@/contexts/useMain";
import { useRouter } from "next/navigation"
import { useEffect } from "react";

export default function Login() {
    const router = useRouter();
    const { person } = useMain();

    useEffect(() => {
        console.log(person)
    }, []);

    function handleIndex() {
        router.push("/")
    }
 
    return (
        <div className={style.body}>
            <h1>login</h1>
            <button onClick={handleIndex}>Index</button>
        </div>
    )
}