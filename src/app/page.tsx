import styles from './page.module.css'

import Index from "./index/page";

export default function Home() {
  return (
    <div>
      <Index />
    </div>
  )
}
