"use client"

import style from "@/app/index/page.module.css"
import useMain from "@/contexts/useMain";
import { useRouter } from "next/navigation"

export default function Index() {
    const router = useRouter();
    const {setPerson} = useMain();

    function handleLogin() {
        setPerson({id: 1, name: "name1"})
        router.push("/login");
    }

    return( 
        <div className={style.body}>
            <h1>index</h1>
            <button onClick={handleLogin}>login</button>
        </div>
    )
}