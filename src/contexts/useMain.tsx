"use client"

import { Person } from "@/types/person";
import { PropsWithChildren, createContext, useContext, useState } from "react";

type ContextProps = {
  person: Person | null;
  setPerson(person: Person | null): void;
};

const MainContext = createContext<ContextProps | null>(null);

export default function useMain() {
  const context = useContext(MainContext);

  if (!context) {
    throw new Error("Please use ThemeProvider in parent component");
  }

  return context;
}

export function MainProvider({ children }: PropsWithChildren) {
  const [person, setPerson] = useState<Person | null>(null);

  return (
    <MainContext.Provider value={{ person, setPerson }}>
      {children}
    </MainContext.Provider>
  );
}