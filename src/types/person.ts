
export type Person = {
    id: Number;
    name: String;
}